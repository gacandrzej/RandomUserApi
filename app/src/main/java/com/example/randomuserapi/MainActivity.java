package com.example.randomuserapi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //
        findViewById(R.id.btn_generate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NetworkTask networkTask = new NetworkTask();
                networkTask.execute();
            }
        });

    }
    //--------------------------------------------------------
    class NetworkTask extends AsyncTask<Object,String,Object>{

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p>
         * This will normally run on a background thread. But to better
         * support testing frameworks, it is recommended that this also tolerates
         * direct execution on the foreground thread, as part of the {@link #execute} call.
         * <p>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param objects The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                URL url = new URL("https://randomuser.me/api");

                InputStream inputStream = url.openStream();
                byte [] buffer = new byte[4096];
                StringBuilder stringBuilder = new StringBuilder("");



                while (inputStream.read(buffer)>0){
                    stringBuilder.append(new String(buffer));
                }
                publishProgress(stringBuilder.toString());


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String[] values) {
            // Toast.makeText(MainActivity.this,String.valueOf(values),Toast.LENGTH_SHORT).show();
            Toast.makeText(MainActivity.this,values[0].toString(),Toast.LENGTH_SHORT).show();

           // super.onProgressUpdate(values);
        }
    }


    //----------------------------------------------------------
}